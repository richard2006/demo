FROM golang:alpine
WORKDIR /go/src
COPY main.go /go/src
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .


# build product image
FROM alpine:latest
WORKDIR /root/
COPY --from=0 /go/src/app .

ENTRYPOINT ["./app"]